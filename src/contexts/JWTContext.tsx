import React, { createContext, useEffect, useReducer } from 'react';

// third-party
import { Chance } from 'chance';
import jwtDecode from 'jwt-decode';

// reducer - state management
import { LOGIN, LOGOUT, REGISTER } from 'store/actions';
import accountReducer from 'store/accountReducer';

// project imports
import Loader from 'ui-component/Loader';

// types
import { KeyedObject } from 'types';
import { InitialLoginContextProps, JWTContextType } from 'types/auth';
import { useLoginMutation, useRegisterMutation } from 'generated/graphql';

// constant
const initialState: InitialLoginContextProps = {
    isLoggedIn: false,
    isInitialized: false,
    user: null,
    profile: null,
    userId: null
};

const verifyToken: (st: string) => boolean = (serviceToken) => {
    if (!serviceToken) {
        return false;
    }
    const decoded: KeyedObject = jwtDecode(serviceToken);
    /**
     * Property 'exp' does not exist on type '<T = unknown>(token: string, options?: JwtDecodeOptions | undefined) => T'.
     */
    return decoded.exp > Date.now() / 1000;
};

const setSession = (serviceToken?: string | null) => {
    if (serviceToken) {
        localStorage.setItem('serviceToken', serviceToken);
        // axios.defaults.headers.common.Authorization = `Bearer ${serviceToken}`;
    } else {
        localStorage.removeItem('serviceToken');
        // delete axios.defaults.headers.common.Authorization;
    }
};

// ==============================|| JWT CONTEXT & PROVIDER ||============================== //
const JWTContext = createContext<JWTContextType | null>(null);

export const JWTProvider = ({ children }: { children: React.ReactElement }) => {
    const [state, dispatch] = useReducer(accountReducer, initialState);
    const [loginCall, loginResp] = useLoginMutation();
    const [registerCall, registerResp] = useRegisterMutation();

    useEffect(() => {
        const init = async () => {
            try {
                const serviceToken = window.localStorage.getItem('serviceToken');
                const usr = window.localStorage.getItem('usr');
                if (serviceToken && verifyToken(serviceToken) && usr) {
                    const profile = JSON.parse(usr).profile;
                    if (profile) {
                        setSession(serviceToken);

                        // TODO: get user info
                        // const response = await axios.get('/api/account/me');
                        // const { user } = response.data;
                        dispatch({
                            type: LOGIN,
                            payload: {
                                isLoggedIn: true,
                                profile
                            }
                        });
                    } else {
                        dispatch({
                            type: LOGOUT
                        });
                    }
                } else {
                    dispatch({
                        type: LOGOUT
                    });
                }
            } catch (err) {
                console.error(err);
                dispatch({
                    type: LOGOUT
                });
            }
        };

        init();
    }, []);

    const login = async (phone: string, password: string) => {
        try {
            const response = await loginCall({
                variables: {
                    username: phone,
                    password
                }
            });

            if (response.data?.tokenAuth) {
                const { token, profile, refreshToken } = response.data?.tokenAuth;
                window.localStorage.setItem('usr', JSON.stringify({ token, profile, refreshToken }));
                setSession(token);
                dispatch({
                    type: LOGIN,
                    payload: {
                        isLoggedIn: true,
                        profile
                    }
                });
            }
        } catch (e) {
            // console.log  error

            loginResp.reset();
            console.log(e);
            dispatch({
                type: LOGOUT
            });
        }
    };

    const register = async (email: string, password: string, username: string) => {
        // todo: this flow need to be recode as it not verified
        try {
            const response = await registerCall({
                variables: {
                    email,
                    password,
                    username
                }
            });
            if (response.data?.createUser) {
                const { token, profile, user, refreshToken } = response.data?.createUser;

                setSession(token);
                window.localStorage.setItem('usr', JSON.stringify({ token, profile, user, refreshToken }));
                dispatch({
                    type: REGISTER,
                    payload: {
                        isLoggedIn: true,
                        profile,
                        userId: user
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    const logout = () => {
        setSession(null);
        dispatch({ type: LOGOUT });
    };

    const resetPassword = (email: string) => console.log(email);

    const updateProfile = () => {};

    if (state.isInitialized !== undefined && !state.isInitialized) {
        console.log('not init');
        return <Loader />;
    }
    console.log('init');
    return (
        <JWTContext.Provider value={{ ...state, login, logout, register, resetPassword, updateProfile }}>{children}</JWTContext.Provider>
    );
};

export default JWTContext;
