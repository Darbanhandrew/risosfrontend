import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    /**
     * The `Date` scalar type represents a Date
     * value as specified by
     * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
     */
    Date: any;
    /**
     * The `DateTime` scalar type represents a DateTime
     * value as specified by
     * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
     */
    DateTime: any;
    /**
     * The `GenericScalar` scalar type represents a generic
     * GraphQL scalar value that could be:
     * String, Boolean, Int, Float, List or Object.
     */
    GenericScalar: any;
    /**
     * Allows use of a JSON String for input / output from the GraphQL schema.
     *
     * Use of this type is *not recommended* as you lose the benefits of having a defined, static
     * schema (one of the key benefits of GraphQL).
     */
    JSONString: any;
    /**
     * Create scalar that ignores normal serialization/deserialization, since
     * that will be handled by the multipart request spec
     */
    Upload: any;
};

export type BadColorReason = BusinessLogicNode & {
    __typename?: 'BadColorReason';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    toothSet: ToothConnection;
    updatedAt: Scalars['DateTime'];
};

export type BadColorReasonToothSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    badColorReason_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    before?: InputMaybe<Scalars['String']>;
    cl_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isBadColor_In?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    toothNumber_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    toothService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
};

export type BadColorReasonConnection = {
    __typename?: 'BadColorReasonConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<BadColorReasonEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BadColorReason` and its cursor. */
export type BadColorReasonEdge = {
    __typename?: 'BadColorReasonEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<BadColorReason>;
};

/** An enumeration. */
export enum BusinesslogicOrderStatusChoices {
    /** Accept */
    ProcessingAccept = 'PROCESSING_ACCEPT',
    /** Ceramic */
    ProcessingCeramic = 'PROCESSING_CERAMIC',
    /** Designing */
    ProcessingDesigning = 'PROCESSING_DESIGNING',
    /** Ditch */
    ProcessingDitch = 'PROCESSING_DITCH',
    /** Invoice Ready */
    ProcessingInvoiceReady = 'PROCESSING_INVOICE_READY',
    /** Molding */
    ProcessingMolding = 'PROCESSING_MOLDING',
    /** Print/Milling */
    ProcessingPrintMilling = 'PROCESSING_PRINT_MILLING',
    /** Reject */
    ProcessingReject = 'PROCESSING_REJECT',
    /** Reject And Resend */
    ProcessingRejectAndResend = 'PROCESSING_REJECT_AND_RESEND',
    /** Scan */
    ProcessingScan = 'PROCESSING_SCAN',
    /** Water Wax */
    ProcessingWaterWax = 'PROCESSING_WATER_WAX',
    /** ready */
    Ready = 'READY',
    /** Sent */
    Sent = 'SENT'
}

/** An enumeration. */
export enum BusinesslogicTicketMessgaeStatusChoices {
    /** Read */
    Read = 'READ',
    /** Unread */
    Unread = 'UNREAD'
}

export type ChangePassword = {
    __typename?: 'ChangePassword';
    status?: Maybe<Scalars['String']>;
};

export type CreateDevice = {
    __typename?: 'CreateDevice';
    status?: Maybe<Scalars['String']>;
};

export type CreateInvoiceInput = {
    actualDate?: InputMaybe<Scalars['Date']>;
    clientMutationId?: InputMaybe<Scalars['String']>;
    description?: InputMaybe<Scalars['String']>;
    id?: InputMaybe<Scalars['ID']>;
    price?: InputMaybe<Scalars['Float']>;
    recieptImage?: InputMaybe<Scalars['String']>;
    relatedOrder: Scalars['ID'];
};

export type CreateInvoicePayload = {
    __typename?: 'CreateInvoicePayload';
    clientMutationId?: Maybe<Scalars['String']>;
    errors?: Maybe<Array<Maybe<ErrorType>>>;
    invoice?: Maybe<InvoiceType>;
};

export type CreateLab = {
    __typename?: 'CreateLab';
    profile?: Maybe<Scalars['String']>;
    refreshToken?: Maybe<Scalars['String']>;
    token?: Maybe<Scalars['String']>;
    user?: Maybe<Scalars['String']>;
};

export type CreateOrderInput = {
    clientMutationId?: InputMaybe<Scalars['String']>;
    description?: InputMaybe<Scalars['String']>;
    expectedDate?: InputMaybe<Scalars['Date']>;
    finalizedLab?: InputMaybe<Scalars['ID']>;
    relatedService?: InputMaybe<Scalars['ID']>;
    status: Scalars['String'];
};

export type CreateOrderPayload = {
    __typename?: 'CreateOrderPayload';
    clientMutationId?: Maybe<Scalars['String']>;
    errors?: Maybe<Array<Maybe<ErrorType>>>;
    order?: Maybe<OrderType>;
    service?: Maybe<OrderType>;
};

export type CreatePatient = {
    __typename?: 'CreatePatient';
    profile?: Maybe<Scalars['String']>;
    refreshToken?: Maybe<Scalars['String']>;
    token?: Maybe<Scalars['String']>;
    user?: Maybe<Scalars['String']>;
};

export type CreateServiceInput = {
    category?: InputMaybe<Scalars['ID']>;
    centralSize?: InputMaybe<Scalars['Int']>;
    clientMutationId?: InputMaybe<Scalars['String']>;
    relatedDoctor: Scalars['ID'];
    relatedPatient: Scalars['ID'];
    relatedSmileDesign?: InputMaybe<Scalars['ID']>;
};

export type CreateServicePayload = {
    __typename?: 'CreateServicePayload';
    clientMutationId?: Maybe<Scalars['String']>;
    errors?: Maybe<Array<Maybe<ErrorType>>>;
    service?: Maybe<ServiceType>;
};

export type CreateTicket = {
    __typename?: 'CreateTicket';
    status?: Maybe<Scalars['String']>;
};

export type CreateUser = {
    __typename?: 'CreateUser';
    profile?: Maybe<Scalars['String']>;
    refreshToken?: Maybe<Scalars['String']>;
    token?: Maybe<Scalars['String']>;
    user?: Maybe<Scalars['String']>;
};

export type DashboardMessage = ExtendProfileNode & {
    __typename?: 'DashboardMessage';
    _id?: Maybe<Scalars['Int']>;
    /** The ID of the object */
    id: Scalars['ID'];
    message: Scalars['String'];
};

export type DashboardMessageConnection = {
    __typename?: 'DashboardMessageConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<DashboardMessageEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `DashboardMessage` and its cursor. */
export type DashboardMessageEdge = {
    __typename?: 'DashboardMessageEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<DashboardMessage>;
};

export type DeletePatient = {
    __typename?: 'DeletePatient';
    status?: Maybe<Scalars['String']>;
};

export type DeletePatientPic = {
    __typename?: 'DeletePatientPic';
    status?: Maybe<Scalars['String']>;
};

export type Doctor = BusinessLogicNode & {
    __typename?: 'Doctor';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    patientSet: Array<PatientType>;
    rateSize: Scalars['Int'];
    rating: Scalars['Float'];
    relatedProfile: Profile;
    services: Array<ServiceType>;
    smileDesigns: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type DoctorSmileDesignsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type DoctorConnection = {
    __typename?: 'DoctorConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<DoctorEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Doctor` and its cursor. */
export type DoctorEdge = {
    __typename?: 'DoctorEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Doctor>;
};

export type DoctorNode = {
    __typename?: 'DoctorNode';
    createdAt: Scalars['DateTime'];
    id: Scalars['ID'];
    patientSet: Array<PatientType>;
    rateSize: Scalars['Int'];
    rating: Scalars['Float'];
    relatedProfile: Profile;
    services: Array<ServiceType>;
    smileDesigns: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type DoctorNodeSmileDesignsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type ErrorType = {
    __typename?: 'ErrorType';
    field: Scalars['String'];
    messages: Array<Scalars['String']>;
};

/** An enumeration. */
export enum ExtendprofileProfileGenderChoices {
    /** Female */
    Female = 'FEMALE',
    /** Male */
    Male = 'MALE',
    /** Unknown */
    Unknown = 'UNKNOWN'
}

/** An enumeration. */
export enum ExtendprofileProfileRoleChoices {
    /** Doctor */
    Doctor = 'DOCTOR',
    /** Lab */
    Lab = 'LAB',
    /** Patient */
    Patient = 'PATIENT'
}

/** An enumeration. */
export enum ExtendprofileProfileStatusChoices {
    /** Active */
    Active = 'ACTIVE',
    /** Banned */
    Banned = 'BANNED',
    /** Deactive */
    Deactive = 'DEACTIVE',
    /** Freetrial */
    Freetrial = 'FREETRIAL'
}

export type FaceShape = SmileDesignNode & {
    __typename?: 'FaceShape';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    smiledesignserviceSet: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type FaceShapeSmiledesignserviceSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type FaceShapeConnection = {
    __typename?: 'FaceShapeConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<FaceShapeEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `FaceShape` and its cursor. */
export type FaceShapeEdge = {
    __typename?: 'FaceShapeEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<FaceShape>;
};

export type ForgetPass = {
    __typename?: 'ForgetPass';
    status?: Maybe<Scalars['String']>;
};

export type Invoice = BusinessLogicNode & {
    __typename?: 'Invoice';
    _id?: Maybe<Scalars['Int']>;
    actualDate?: Maybe<Scalars['Date']>;
    createdAt: Scalars['DateTime'];
    description?: Maybe<Scalars['String']>;
    /** The ID of the object */
    id: Scalars['ID'];
    price?: Maybe<Scalars['Float']>;
    recieptImage?: Maybe<Scalars['String']>;
    relatedOrder: OrderType;
    updatedAt: Scalars['DateTime'];
};

export type InvoiceConnection = {
    __typename?: 'InvoiceConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<InvoiceEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Invoice` and its cursor. */
export type InvoiceEdge = {
    __typename?: 'InvoiceEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Invoice>;
};

export type InvoiceType = {
    __typename?: 'InvoiceType';
    actualDate?: Maybe<Scalars['Date']>;
    createdAt: Scalars['DateTime'];
    description?: Maybe<Scalars['String']>;
    id: Scalars['ID'];
    price?: Maybe<Scalars['Float']>;
    recieptImage?: Maybe<Scalars['String']>;
    relatedOrder: OrderType;
    updatedAt: Scalars['DateTime'];
};

export type Lab = BusinessLogicNode & {
    __typename?: 'Lab';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    labPics?: Maybe<LabPic>;
    orderSet: Array<OrderType>;
    rateSize: Scalars['Int'];
    rating: Scalars['Float'];
    relatedProfile: Profile;
    updatedAt: Scalars['DateTime'];
};

export type LabConnection = {
    __typename?: 'LabConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<LabEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Lab` and its cursor. */
export type LabEdge = {
    __typename?: 'LabEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Lab>;
};

export type LabNode = {
    __typename?: 'LabNode';
    createdAt: Scalars['DateTime'];
    id: Scalars['ID'];
    labPics?: Maybe<LabPic>;
    orderSet: Array<OrderType>;
    rateSize: Scalars['Int'];
    rating: Scalars['Float'];
    relatedProfile: Profile;
    updatedAt: Scalars['DateTime'];
};

export type LabPic = BusinessLogicNode & {
    __typename?: 'LabPic';
    _id?: Maybe<Scalars['Int']>;
    createdAt?: Maybe<Scalars['DateTime']>;
    /** The ID of the object */
    id: Scalars['ID'];
    lab: LabNode;
    number?: Maybe<Scalars['Int']>;
    pic?: Maybe<Scalars['String']>;
    updatedAt?: Maybe<Scalars['DateTime']>;
};

export type LabPicConnection = {
    __typename?: 'LabPicConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<LabPicEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `LabPic` and its cursor. */
export type LabPicEdge = {
    __typename?: 'LabPicEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<LabPic>;
};

export type LabPicMutation = {
    __typename?: 'LabPicMutation';
    status?: Maybe<Scalars['String']>;
};

export type LocationMutation = {
    __typename?: 'LocationMutation';
    status?: Maybe<Scalars['String']>;
};

export type Log = BusinessLogicNode & {
    __typename?: 'Log';
    _id?: Maybe<Scalars['Int']>;
    createdAt?: Maybe<Scalars['DateTime']>;
    /** The ID of the object */
    id: Scalars['ID'];
    message?: Maybe<Scalars['String']>;
    relatedOrder: OrderType;
    status: Scalars['String'];
};

export type LogConnection = {
    __typename?: 'LogConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<LogEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Log` and its cursor. */
export type LogEdge = {
    __typename?: 'LogEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Log>;
};

export type Mutations = {
    __typename?: 'Mutations';
    changePassword?: Maybe<ChangePassword>;
    createDevice?: Maybe<CreateDevice>;
    createInvoice?: Maybe<CreateInvoicePayload>;
    createLab?: Maybe<CreateLab>;
    createOrder?: Maybe<CreateOrderPayload>;
    createPatient?: Maybe<CreatePatient>;
    createService?: Maybe<CreateServicePayload>;
    createTicker?: Maybe<CreateTicket>;
    createUser?: Maybe<CreateUser>;
    deletePatient?: Maybe<DeletePatient>;
    deletePatientPic?: Maybe<DeletePatientPic>;
    forgetPass?: Maybe<ForgetPass>;
    labpicMutation?: Maybe<LabPicMutation>;
    locationMutation?: Maybe<LocationMutation>;
    refreshToken?: Maybe<Refresh>;
    requestOtp?: Maybe<RequestOtp>;
    tokenAuth?: Maybe<ObtainJsonWebToken>;
    toothMutation?: Maybe<ToothMutation>;
    toothMutationJson?: Maybe<ToothMutationJson>;
    updateLabRate?: Maybe<UpdateLabRate>;
    updateOrder?: Maybe<UpdateOrder>;
    updatePatientPic?: Maybe<UpdatePatientPic>;
    updateProfile?: Maybe<UpdateProfile>;
    updateSmileDesign?: Maybe<UpdateSmileDesign>;
    verifyToken?: Maybe<Verify>;
    verifyUser?: Maybe<VerifyUser>;
};

export type MutationsChangePasswordArgs = {
    newPassword: Scalars['String'];
    username: Scalars['String'];
};

export type MutationsCreateDeviceArgs = {
    ProfileId: Scalars['ID'];
    deviceId: Scalars['String'];
};

export type MutationsCreateInvoiceArgs = {
    input: CreateInvoiceInput;
};

export type MutationsCreateLabArgs = {
    address?: InputMaybe<Scalars['String']>;
    description?: InputMaybe<Scalars['String']>;
    name?: InputMaybe<Scalars['String']>;
    phoneNumber: Scalars['String'];
    telephoneNumber?: InputMaybe<Scalars['String']>;
};

export type MutationsCreateOrderArgs = {
    input: CreateOrderInput;
};

export type MutationsCreatePatientArgs = {
    address?: InputMaybe<Scalars['String']>;
    age?: InputMaybe<Scalars['Int']>;
    description?: InputMaybe<Scalars['String']>;
    email?: InputMaybe<Scalars['String']>;
    name?: InputMaybe<Scalars['String']>;
    password?: InputMaybe<Scalars['String']>;
    patientPics?: InputMaybe<PatientPics>;
    phoneNumber: Scalars['String'];
    profileDoctorId?: InputMaybe<Scalars['Int']>;
};

export type MutationsCreateServiceArgs = {
    input: CreateServiceInput;
};

export type MutationsCreateTickerArgs = {
    message: Scalars['String'];
    receiverProfile: Scalars['Int'];
    relatedOrder: Scalars['Int'];
    senderProfile: Scalars['Int'];
};

export type MutationsCreateUserArgs = {
    email?: InputMaybe<Scalars['String']>;
    password: Scalars['String'];
    username: Scalars['String'];
};

export type MutationsDeletePatientArgs = {
    patientId: Scalars['Int'];
};

export type MutationsDeletePatientPicArgs = {
    patientId: Scalars['Int'];
    selectedFields?: InputMaybe<PatientPicsDeletion>;
};

export type MutationsForgetPassArgs = {
    phoneNumber: Scalars['String'];
};

export type MutationsLabpicMutationArgs = {
    labId: Scalars['ID'];
    pic: Scalars['Upload'];
    picNumber: Scalars['Int'];
};

export type MutationsLocationMutationArgs = {
    latitude: Scalars['Float'];
    longitude: Scalars['Float'];
    profileId: Scalars['Int'];
};

export type MutationsRefreshTokenArgs = {
    refreshToken?: InputMaybe<Scalars['String']>;
};

export type MutationsRequestOtpArgs = {
    username: Scalars['String'];
};

export type MutationsTokenAuthArgs = {
    password: Scalars['String'];
    username: Scalars['String'];
};

export type MutationsToothMutationArgs = {
    relatedService: Scalars['ID'];
    teeth?: InputMaybe<Array<InputMaybe<TeethInput>>>;
};

export type MutationsToothMutationJsonArgs = {
    centralSize?: InputMaybe<Scalars['String']>;
    jsonObject?: InputMaybe<Scalars['JSONString']>;
    relatedService: Scalars['ID'];
};

export type MutationsUpdateLabRateArgs = {
    labId: Scalars['Int'];
    rate: Scalars['Float'];
};

export type MutationsUpdateOrderArgs = {
    description?: InputMaybe<Scalars['String']>;
    expectedDate?: InputMaybe<Scalars['DateTime']>;
    finalizedLab?: InputMaybe<Scalars['ID']>;
    orderId: Scalars['ID'];
    relatedService?: InputMaybe<Scalars['ID']>;
    status?: InputMaybe<Scalars['String']>;
};

export type MutationsUpdatePatientPicArgs = {
    doctorId?: InputMaybe<Scalars['Int']>;
    patientId: Scalars['Int'];
    patientPics: PatientPics;
};

export type MutationsUpdateProfileArgs = {
    address?: InputMaybe<Scalars['String']>;
    age?: InputMaybe<Scalars['Int']>;
    description?: InputMaybe<Scalars['String']>;
    email?: InputMaybe<Scalars['String']>;
    firstName?: InputMaybe<Scalars['String']>;
    gender?: InputMaybe<Scalars['String']>;
    id: Scalars['ID'];
    lastName?: InputMaybe<Scalars['String']>;
    phoneNumber?: InputMaybe<Scalars['String']>;
    profilePic?: InputMaybe<Scalars['Upload']>;
    status?: InputMaybe<Scalars['String']>;
    telephoneNumber?: InputMaybe<Scalars['String']>;
};

export type MutationsUpdateSmileDesignArgs = {
    doctorId: Scalars['Int'];
    patientId: Scalars['Int'];
    smileCategory?: InputMaybe<Scalars['String']>;
    smileColor?: InputMaybe<Scalars['String']>;
    smileDesignImages?: InputMaybe<SmileDesignImages>;
    smileDesignState?: InputMaybe<Scalars['String']>;
};

export type MutationsVerifyTokenArgs = {
    token?: InputMaybe<Scalars['String']>;
};

export type MutationsVerifyUserArgs = {
    otpMessage: Scalars['String'];
    username: Scalars['String'];
};

export type NotifReceiver = NotificationNode & {
    __typename?: 'NotifReceiver';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    deviceId: Scalars['String'];
    /** The ID of the object */
    id: Scalars['ID'];
    notifications: NotificationConnection;
    profile?: Maybe<Profile>;
};

export type NotifReceiverNotificationsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receivers_In?: InputMaybe<Array<InputMaybe<Array<InputMaybe<Scalars['ID']>>>>>;
    reportUrl_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type NotifReceiverConnection = {
    __typename?: 'NotifReceiverConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<NotifReceiverEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `NotifReceiver` and its cursor. */
export type NotifReceiverEdge = {
    __typename?: 'NotifReceiverEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<NotifReceiver>;
};

export type NotifService = NotificationNode & {
    __typename?: 'NotifService';
    _id?: Maybe<Scalars['Int']>;
    /** The ID of the object */
    id: Scalars['ID'];
    notificationSet: NotificationConnection;
    objectId: Scalars['Int'];
    objectType: NotificationNotifServiceObjectTypeChoices;
};

export type NotifServiceNotificationSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receivers_In?: InputMaybe<Array<InputMaybe<Array<InputMaybe<Scalars['ID']>>>>>;
    reportUrl_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type NotifServiceConnection = {
    __typename?: 'NotifServiceConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<NotifServiceEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `NotifService` and its cursor. */
export type NotifServiceEdge = {
    __typename?: 'NotifServiceEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<NotifService>;
};

export type Notification = NotificationNode & {
    __typename?: 'Notification';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    message: Scalars['String'];
    notifId?: Maybe<Scalars['String']>;
    notifService?: Maybe<NotifService>;
    receivers: NotifReceiverConnection;
    reportUrl?: Maybe<Scalars['String']>;
    status?: Maybe<NotificationNotificationStatusChoices>;
};

export type NotificationReceiversArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    deviceId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    notifications_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    profile_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type NotificationConnection = {
    __typename?: 'NotificationConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<NotificationEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `Notification` and its cursor. */
export type NotificationEdge = {
    __typename?: 'NotificationEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Notification>;
};

/** An enumeration. */
export enum NotificationNotifServiceObjectTypeChoices {
    /** INVOICE */
    Invoice = 'INVOICE',
    /** ORDER */
    Order = 'ORDER'
}

/** An enumeration. */
export enum NotificationNotificationStatusChoices {
    /** FAILED */
    Failed = 'FAILED',
    /** SUCCESS */
    Success = 'SUCCESS'
}

export type Otp = ExtendProfileNode & {
    __typename?: 'OTP';
    _id?: Maybe<Scalars['Int']>;
    /** The ID of the object */
    id: Scalars['ID'];
    isValid: Scalars['Boolean'];
    message?: Maybe<Scalars['String']>;
    profile?: Maybe<Profile>;
};

export type OtpConnection = {
    __typename?: 'OTPConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<OtpEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `OTP` and its cursor. */
export type OtpEdge = {
    __typename?: 'OTPEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Otp>;
};

export type ObtainJsonWebToken = {
    __typename?: 'ObtainJSONWebToken';
    payload: Scalars['GenericScalar'];
    profile?: Maybe<Scalars['String']>;
    refreshExpiresIn: Scalars['Int'];
    refreshToken: Scalars['String'];
    token: Scalars['String'];
};

export type Order = BusinessLogicNode & {
    __typename?: 'Order';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    description?: Maybe<Scalars['String']>;
    expectedDate?: Maybe<Scalars['Date']>;
    finalizedLab?: Maybe<LabNode>;
    /** The ID of the object */
    id: Scalars['ID'];
    invoice?: Maybe<InvoiceType>;
    logs: LogConnection;
    relatedService?: Maybe<ServiceType>;
    status: BusinesslogicOrderStatusChoices;
    ticketSet: TicketConnection;
    updatedAt: Scalars['Date'];
};

export type OrderLogsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type OrderTicketSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    messgaeStatus_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type OrderConnection = {
    __typename?: 'OrderConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<OrderEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Order` and its cursor. */
export type OrderEdge = {
    __typename?: 'OrderEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Order>;
};

export type OrderType = {
    __typename?: 'OrderType';
    createdAt: Scalars['DateTime'];
    description?: Maybe<Scalars['String']>;
    expectedDate?: Maybe<Scalars['Date']>;
    finalizedLab?: Maybe<LabNode>;
    id: Scalars['ID'];
    invoice?: Maybe<InvoiceType>;
    logs: LogConnection;
    relatedService?: Maybe<ServiceType>;
    status: BusinesslogicOrderStatusChoices;
    ticketSet: TicketConnection;
    updatedAt: Scalars['Date'];
};

export type OrderTypeLogsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type OrderTypeTicketSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    messgaeStatus_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

/** The Relay compliant `PageInfo` type, containing data necessary to paginate this connection. */
export type PageInfo = {
    __typename?: 'PageInfo';
    /** When paginating forwards, the cursor to continue. */
    endCursor?: Maybe<Scalars['String']>;
    /** When paginating forwards, are there more items? */
    hasNextPage: Scalars['Boolean'];
    /** When paginating backwards, are there more items? */
    hasPreviousPage: Scalars['Boolean'];
    /** When paginating backwards, the cursor to continue. */
    startCursor?: Maybe<Scalars['String']>;
};

export type Patient = BusinessLogicNode & {
    __typename?: 'Patient';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    deletedAt?: Maybe<Scalars['DateTime']>;
    doctor: Array<DoctorNode>;
    /** The ID of the object */
    id: Scalars['ID'];
    patientPic?: Maybe<PatientPic>;
    relatedProfile: Profile;
    serviceSet: Array<ServiceType>;
    smileDesigns: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type PatientSmileDesignsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type PatientConnection = {
    __typename?: 'PatientConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<PatientEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Patient` and its cursor. */
export type PatientEdge = {
    __typename?: 'PatientEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Patient>;
};

export type PatientPic = BusinessLogicNode & {
    __typename?: 'PatientPic';
    _id?: Maybe<Scalars['Int']>;
    createdAt?: Maybe<Scalars['DateTime']>;
    fullSmileImage?: Maybe<Scalars['String']>;
    /** The ID of the object */
    id: Scalars['ID'];
    optionalImage?: Maybe<Scalars['String']>;
    patient?: Maybe<PatientType>;
    sideImage?: Maybe<Scalars['String']>;
    smileImage?: Maybe<Scalars['String']>;
    updatedAt?: Maybe<Scalars['DateTime']>;
};

export type PatientPicConnection = {
    __typename?: 'PatientPicConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<PatientPicEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `PatientPic` and its cursor. */
export type PatientPicEdge = {
    __typename?: 'PatientPicEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<PatientPic>;
};

export type PatientType = {
    __typename?: 'PatientType';
    createdAt: Scalars['DateTime'];
    deletedAt?: Maybe<Scalars['DateTime']>;
    doctor: Array<DoctorNode>;
    id: Scalars['ID'];
    patientPic?: Maybe<PatientPic>;
    relatedProfile: Profile;
    serviceSet: Array<ServiceType>;
    smileDesigns: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type PatientTypeSmileDesignsArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type Profile = ExtendProfileNode & {
    __typename?: 'Profile';
    OTP: OtpConnection;
    _id?: Maybe<Scalars['Int']>;
    address?: Maybe<Scalars['String']>;
    age?: Maybe<Scalars['Int']>;
    city?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
    doctor?: Maybe<DoctorNode>;
    email?: Maybe<Scalars['String']>;
    firstName?: Maybe<Scalars['String']>;
    gender?: Maybe<ExtendprofileProfileGenderChoices>;
    /** The ID of the object */
    id: Scalars['ID'];
    lab?: Maybe<LabNode>;
    lastName?: Maybe<Scalars['String']>;
    notifreceiver?: Maybe<NotifReceiver>;
    patient?: Maybe<PatientType>;
    phoneNumber?: Maybe<Scalars['String']>;
    profilePic?: Maybe<Scalars['String']>;
    receiver: TicketConnection;
    role: ExtendprofileProfileRoleChoices;
    sender: TicketConnection;
    status: ExtendprofileProfileStatusChoices;
    telephoneNumber?: Maybe<Scalars['String']>;
};

export type ProfileOtpArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isValid?: InputMaybe<Scalars['Boolean']>;
    isValid_In?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    profile?: InputMaybe<Scalars['ID']>;
    profile_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type ProfileReceiverArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    messgaeStatus_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type ProfileSenderArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    messgaeStatus_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type ProfileConnection = {
    __typename?: 'ProfileConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<ProfileEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `Profile` and its cursor. */
export type ProfileEdge = {
    __typename?: 'ProfileEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Profile>;
};

export type Query = {
    __typename?: 'Query';
    BadColorReason?: Maybe<BadColorReason>;
    DashboardMessage?: Maybe<DashboardMessage>;
    Doctor?: Maybe<Doctor>;
    FaceShape?: Maybe<FaceShape>;
    Invoice?: Maybe<Invoice>;
    Lab?: Maybe<Lab>;
    LabPic?: Maybe<LabPic>;
    Log?: Maybe<Log>;
    NotifReceiver?: Maybe<NotifReceiver>;
    NotifService?: Maybe<NotifService>;
    Notification?: Maybe<Notification>;
    OTP?: Maybe<Otp>;
    Order?: Maybe<Order>;
    Patient?: Maybe<Patient>;
    PatientPic?: Maybe<PatientPic>;
    Profile?: Maybe<Profile>;
    Service?: Maybe<Service>;
    ServiceCategory?: Maybe<ServiceCategory>;
    SmileCategory?: Maybe<SmileCategory>;
    SmileColor?: Maybe<SmileColor>;
    SmileDesignService?: Maybe<SmileDesignService>;
    Ticket?: Maybe<Ticket>;
    Tooth?: Maybe<Tooth>;
    ToothSevice?: Maybe<ToothSevice>;
    allBadcolorreason?: Maybe<BadColorReasonConnection>;
    allDashboardmessage?: Maybe<DashboardMessageConnection>;
    allDoctor?: Maybe<DoctorConnection>;
    allFaceshape?: Maybe<FaceShapeConnection>;
    allInvoice?: Maybe<InvoiceConnection>;
    allLab?: Maybe<LabConnection>;
    allLabpic?: Maybe<LabPicConnection>;
    allLog?: Maybe<LogConnection>;
    allNotification?: Maybe<NotificationConnection>;
    allNotifreceiver?: Maybe<NotifReceiverConnection>;
    allNotifservice?: Maybe<NotifServiceConnection>;
    allOrder?: Maybe<OrderConnection>;
    allOtp?: Maybe<OtpConnection>;
    allPatient?: Maybe<PatientConnection>;
    allPatientpic?: Maybe<PatientPicConnection>;
    allProfile?: Maybe<ProfileConnection>;
    allService?: Maybe<ServiceConnection>;
    allServicecategory?: Maybe<ServiceCategoryConnection>;
    allSmilecategory?: Maybe<SmileCategoryConnection>;
    allSmilecolor?: Maybe<SmileColorConnection>;
    allSmiledesignservice?: Maybe<SmileDesignServiceConnection>;
    allTicket?: Maybe<TicketConnection>;
    allTooth?: Maybe<ToothConnection>;
    allToothsevice?: Maybe<ToothSeviceConnection>;
    getNearestDoctor?: Maybe<Array<Maybe<DoctorNode>>>;
    getNearestLab?: Maybe<Array<Maybe<LabNode>>>;
    getReport?: Maybe<Array<Maybe<ReportNode>>>;
};

export type QueryBadColorReasonArgs = {
    id: Scalars['ID'];
};

export type QueryDashboardMessageArgs = {
    id: Scalars['ID'];
};

export type QueryDoctorArgs = {
    id: Scalars['ID'];
};

export type QueryFaceShapeArgs = {
    id: Scalars['ID'];
};

export type QueryInvoiceArgs = {
    id: Scalars['ID'];
};

export type QueryLabArgs = {
    id: Scalars['ID'];
};

export type QueryLabPicArgs = {
    id: Scalars['ID'];
};

export type QueryLogArgs = {
    id: Scalars['ID'];
};

export type QueryNotifReceiverArgs = {
    id: Scalars['ID'];
};

export type QueryNotifServiceArgs = {
    id: Scalars['ID'];
};

export type QueryNotificationArgs = {
    id: Scalars['ID'];
};

export type QueryOtpArgs = {
    id: Scalars['ID'];
};

export type QueryOrderArgs = {
    id: Scalars['ID'];
};

export type QueryPatientArgs = {
    id: Scalars['ID'];
};

export type QueryPatientPicArgs = {
    id: Scalars['ID'];
};

export type QueryProfileArgs = {
    id: Scalars['ID'];
};

export type QueryServiceArgs = {
    id: Scalars['ID'];
};

export type QueryServiceCategoryArgs = {
    id: Scalars['ID'];
};

export type QuerySmileCategoryArgs = {
    id: Scalars['ID'];
};

export type QuerySmileColorArgs = {
    id: Scalars['ID'];
};

export type QuerySmileDesignServiceArgs = {
    id: Scalars['ID'];
};

export type QueryTicketArgs = {
    id: Scalars['ID'];
};

export type QueryToothArgs = {
    id: Scalars['ID'];
};

export type QueryToothSeviceArgs = {
    id: Scalars['ID'];
};

export type QueryAllBadcolorreasonArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    tooth?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    tooth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllDashboardmessageArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
};

export type QueryAllDoctorArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    rateSize?: InputMaybe<Scalars['Int']>;
    rateSize_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    rating?: InputMaybe<Scalars['Float']>;
    rating_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedProfile?: InputMaybe<Scalars['ID']>;
    relatedProfile_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    services?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    services_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    smileDesigns?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    smileDesigns_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    sortByRate?: InputMaybe<Scalars['String']>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllFaceshapeArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    smiledesignservice?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    smiledesignservice_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllInvoiceArgs = {
    actualDate?: InputMaybe<Scalars['Date']>;
    actualDate_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    description?: InputMaybe<Scalars['String']>;
    description_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    price?: InputMaybe<Scalars['Float']>;
    price_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedOrder?: InputMaybe<Scalars['ID']>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllLabArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    labPics?: InputMaybe<Scalars['ID']>;
    labPics_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    order_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    rateSize?: InputMaybe<Scalars['Int']>;
    rateSize_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    rating?: InputMaybe<Scalars['Float']>;
    rating_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedProfile?: InputMaybe<Scalars['ID']>;
    relatedProfile_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    searchByName?: InputMaybe<Scalars['String']>;
    sortByRate?: InputMaybe<Scalars['String']>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllLabpicArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    lab?: InputMaybe<Scalars['ID']>;
    lab_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    number?: InputMaybe<Scalars['Int']>;
    number_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllLogArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedOrder?: InputMaybe<Scalars['ID']>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status?: InputMaybe<Scalars['String']>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllNotificationArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifId?: InputMaybe<Scalars['String']>;
    notifId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifService?: InputMaybe<Scalars['ID']>;
    notifService_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receivers?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    receivers_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    reportUrl?: InputMaybe<Scalars['String']>;
    reportUrl_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status?: InputMaybe<Scalars['String']>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllNotifreceiverArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    deviceId?: InputMaybe<Scalars['String']>;
    deviceId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    notifications?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    notifications_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    profile?: InputMaybe<Scalars['ID']>;
    profile_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllNotifserviceArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    notification?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    notification_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    objectId?: InputMaybe<Scalars['Int']>;
    objectId_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    objectType?: InputMaybe<Scalars['String']>;
    objectType_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
};

export type QueryAllOrderArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    description?: InputMaybe<Scalars['String']>;
    description_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    doctorId?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    expectedDate?: InputMaybe<Scalars['Date']>;
    expectedDate_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    finalizedLab?: InputMaybe<Scalars['ID']>;
    finalizedLab_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    invoice?: InputMaybe<Scalars['ID']>;
    invoice_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    logs?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    logs_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService?: InputMaybe<Scalars['ID']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    searchByName?: InputMaybe<Scalars['String']>;
    status?: InputMaybe<Scalars['String']>;
    statusStartwith?: InputMaybe<Scalars['String']>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    ticket?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    ticket_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['Date']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllOtpArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isValid?: InputMaybe<Scalars['Boolean']>;
    isValid_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    profile?: InputMaybe<Scalars['ID']>;
    profile_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllPatientArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    deletedAt?: InputMaybe<Scalars['DateTime']>;
    deletedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    doctor?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patientPic?: InputMaybe<Scalars['ID']>;
    patientPic_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    profileId?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedProfile?: InputMaybe<Scalars['ID']>;
    relatedProfile_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    searchByName?: InputMaybe<Scalars['String']>;
    service?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    smileDesigns?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    smileDesigns_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllPatientpicArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient?: InputMaybe<Scalars['ID']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllProfileArgs = {
    OTP?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    OTP_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    address?: InputMaybe<Scalars['String']>;
    address_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    after?: InputMaybe<Scalars['String']>;
    age?: InputMaybe<Scalars['Int']>;
    age_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    before?: InputMaybe<Scalars['String']>;
    city?: InputMaybe<Scalars['String']>;
    city_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    description?: InputMaybe<Scalars['String']>;
    description_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    doctor?: InputMaybe<Scalars['ID']>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    email?: InputMaybe<Scalars['String']>;
    email_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    firstName?: InputMaybe<Scalars['String']>;
    firstName_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    gender?: InputMaybe<Scalars['String']>;
    gender_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    lab?: InputMaybe<Scalars['ID']>;
    lab_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    lastName?: InputMaybe<Scalars['String']>;
    lastName_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    notifreceiver?: InputMaybe<Scalars['ID']>;
    notifreceiver_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    patient?: InputMaybe<Scalars['ID']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    phoneNumber?: InputMaybe<Scalars['String']>;
    phoneNumber_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    receiver?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    role?: InputMaybe<Scalars['String']>;
    role_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    sender?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status?: InputMaybe<Scalars['String']>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    telephoneNumber?: InputMaybe<Scalars['String']>;
    telephoneNumber_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    user?: InputMaybe<Scalars['ID']>;
    user_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllServiceArgs = {
    Teeth?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    Teeth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    category?: InputMaybe<Scalars['ID']>;
    category_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    centralSize?: InputMaybe<Scalars['Int']>;
    centralSize_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    orders?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    orders_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedDoctor?: InputMaybe<Scalars['ID']>;
    relatedDoctor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedPatient?: InputMaybe<Scalars['ID']>;
    relatedPatient_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedSmileDesign?: InputMaybe<Scalars['ID']>;
    relatedSmileDesign_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllServicecategoryArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    depth?: InputMaybe<Scalars['Int']>;
    depth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    numchild?: InputMaybe<Scalars['Int']>;
    numchild_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    path?: InputMaybe<Scalars['String']>;
    path_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    service?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllSmilecategoryArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    depth?: InputMaybe<Scalars['Int']>;
    depth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    numchild?: InputMaybe<Scalars['Int']>;
    numchild_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    path?: InputMaybe<Scalars['String']>;
    path_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    smiledesignservice?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    smiledesignservice_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllSmilecolorArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    depth?: InputMaybe<Scalars['Int']>;
    depth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    numchild?: InputMaybe<Scalars['Int']>;
    numchild_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    path?: InputMaybe<Scalars['String']>;
    path_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    smiledesignservice?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    smiledesignservice_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllSmiledesignserviceArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    doctor?: InputMaybe<Scalars['ID']>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient?: InputMaybe<Scalars['ID']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedSmileCategory?: InputMaybe<Scalars['ID']>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedSmileColor?: InputMaybe<Scalars['ID']>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    service?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape?: InputMaybe<Scalars['ID']>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    status?: InputMaybe<Scalars['String']>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    width?: InputMaybe<Scalars['Int']>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllTicketArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    message?: InputMaybe<Scalars['String']>;
    message_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    messgaeStatus?: InputMaybe<Scalars['String']>;
    messgaeStatus_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    receiver?: InputMaybe<Scalars['ID']>;
    receiver_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    relatedOrder?: InputMaybe<Scalars['ID']>;
    relatedOrder_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    sender?: InputMaybe<Scalars['ID']>;
    sender_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllToothArgs = {
    after?: InputMaybe<Scalars['String']>;
    badColorReason?: InputMaybe<Scalars['ID']>;
    badColorReason_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    before?: InputMaybe<Scalars['String']>;
    cl?: InputMaybe<Scalars['Int']>;
    cl_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isBadColor?: InputMaybe<Scalars['Boolean']>;
    isBadColor_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService?: InputMaybe<Scalars['ID']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    toothNumber?: InputMaybe<Scalars['Int']>;
    toothNumber_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    toothService?: InputMaybe<Scalars['ID']>;
    toothService_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryAllToothseviceArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt?: InputMaybe<Scalars['DateTime']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id?: InputMaybe<Scalars['ID']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    name?: InputMaybe<Scalars['String']>;
    name_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    offset?: InputMaybe<Scalars['Int']>;
    tooth?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    tooth_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt?: InputMaybe<Scalars['DateTime']>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type QueryGetNearestDoctorArgs = {
    first?: InputMaybe<Scalars['Int']>;
    profileId?: InputMaybe<Scalars['Int']>;
};

export type QueryGetNearestLabArgs = {
    first?: InputMaybe<Scalars['Int']>;
    profileId?: InputMaybe<Scalars['Int']>;
};

export type QueryGetReportArgs = {
    labId?: InputMaybe<Scalars['Int']>;
};

export type Refresh = {
    __typename?: 'Refresh';
    payload: Scalars['GenericScalar'];
    refreshExpiresIn: Scalars['Int'];
    refreshToken: Scalars['String'];
    token: Scalars['String'];
};

export type ReportNode = {
    __typename?: 'ReportNode';
    name?: Maybe<Scalars['String']>;
    number?: Maybe<Scalars['Int']>;
};

export type RequestOtp = {
    __typename?: 'RequestOTP';
    status?: Maybe<Scalars['String']>;
};

export type Service = BusinessLogicNode & {
    __typename?: 'Service';
    Teeth: ToothConnection;
    _id?: Maybe<Scalars['Int']>;
    category?: Maybe<ServiceCategory>;
    centralSize?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    orders: Array<OrderType>;
    relatedDoctor: DoctorNode;
    relatedPatient: PatientType;
    relatedSmileDesign?: Maybe<SmileDesignService>;
    updatedAt: Scalars['DateTime'];
};

export type ServiceTeethArgs = {
    after?: InputMaybe<Scalars['String']>;
    badColorReason_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    before?: InputMaybe<Scalars['String']>;
    cl_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isBadColor_In?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    toothNumber_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    toothService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
};

export type ServiceCategory = BusinessLogicNode & {
    __typename?: 'ServiceCategory';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    depth: Scalars['Int'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    numchild: Scalars['Int'];
    path: Scalars['String'];
    serviceSet: Array<ServiceType>;
    updatedAt: Scalars['DateTime'];
};

export type ServiceCategoryConnection = {
    __typename?: 'ServiceCategoryConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<ServiceCategoryEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ServiceCategory` and its cursor. */
export type ServiceCategoryEdge = {
    __typename?: 'ServiceCategoryEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<ServiceCategory>;
};

export type ServiceConnection = {
    __typename?: 'ServiceConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<ServiceEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Service` and its cursor. */
export type ServiceEdge = {
    __typename?: 'ServiceEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Service>;
};

export type ServiceType = {
    __typename?: 'ServiceType';
    Teeth: ToothConnection;
    category?: Maybe<ServiceCategory>;
    centralSize?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    id: Scalars['ID'];
    orders: Array<OrderType>;
    relatedDoctor: DoctorNode;
    relatedPatient: PatientType;
    relatedSmileDesign?: Maybe<SmileDesignService>;
    updatedAt: Scalars['DateTime'];
};

export type ServiceTypeTeethArgs = {
    after?: InputMaybe<Scalars['String']>;
    badColorReason_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    before?: InputMaybe<Scalars['String']>;
    cl_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isBadColor_In?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    toothNumber_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    toothService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
};

export type SmileCategory = SmileDesignNode & {
    __typename?: 'SmileCategory';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    depth: Scalars['Int'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    numchild: Scalars['Int'];
    path: Scalars['String'];
    smiledesignserviceSet: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type SmileCategorySmiledesignserviceSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type SmileCategoryConnection = {
    __typename?: 'SmileCategoryConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<SmileCategoryEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `SmileCategory` and its cursor. */
export type SmileCategoryEdge = {
    __typename?: 'SmileCategoryEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<SmileCategory>;
};

export type SmileColor = SmileDesignNode & {
    __typename?: 'SmileColor';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    depth: Scalars['Int'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    numchild: Scalars['Int'];
    path: Scalars['String'];
    smiledesignserviceSet: SmileDesignServiceConnection;
    updatedAt: Scalars['DateTime'];
};

export type SmileColorSmiledesignserviceSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    before?: InputMaybe<Scalars['String']>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    doctor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    first?: InputMaybe<Scalars['Int']>;
    heigth_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    patient_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileCategory_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    relatedSmileColor_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    service_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    shape_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    status_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    width_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type SmileColorConnection = {
    __typename?: 'SmileColorConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<SmileColorEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `SmileColor` and its cursor. */
export type SmileColorEdge = {
    __typename?: 'SmileColorEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<SmileColor>;
};

export type SmileDesignImages = {
    smileImageResult?: InputMaybe<Scalars['Upload']>;
    teethLessImage?: InputMaybe<Scalars['Upload']>;
};

export type SmileDesignService = SmileDesignNode & {
    __typename?: 'SmileDesignService';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    doctor?: Maybe<DoctorNode>;
    heigth?: Maybe<Scalars['Int']>;
    /** The ID of the object */
    id: Scalars['ID'];
    patient?: Maybe<PatientType>;
    relatedSmileCategory?: Maybe<SmileCategory>;
    relatedSmileColor?: Maybe<SmileColor>;
    serviceSet: Array<ServiceType>;
    shape?: Maybe<FaceShape>;
    smileImageResult?: Maybe<Scalars['String']>;
    status: SmiledesignSmileDesignServiceStatusChoices;
    teethLessImage?: Maybe<Scalars['String']>;
    updatedAt: Scalars['DateTime'];
    width?: Maybe<Scalars['Int']>;
};

export type SmileDesignServiceConnection = {
    __typename?: 'SmileDesignServiceConnection';
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<SmileDesignServiceEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
};

/** A Relay edge containing a `SmileDesignService` and its cursor. */
export type SmileDesignServiceEdge = {
    __typename?: 'SmileDesignServiceEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<SmileDesignService>;
};

/** An enumeration. */
export enum SmiledesignSmileDesignServiceStatusChoices {
    /** IMPROPER IMAGE */
    ImproperImage = 'IMPROPER_IMAGE',
    /** NOTREADY */
    Notready = 'NOTREADY',
    /** READY */
    Ready = 'READY'
}

export type TeethInput = {
    cl?: InputMaybe<Scalars['Int']>;
    toothNumber: Scalars['Int'];
    toothService?: InputMaybe<Scalars['String']>;
};

export type Ticket = BusinessLogicNode & {
    __typename?: 'Ticket';
    _id?: Maybe<Scalars['Int']>;
    /** The ID of the object */
    id: Scalars['ID'];
    message: Scalars['String'];
    messgaeStatus: BusinesslogicTicketMessgaeStatusChoices;
    receiver: Profile;
    relatedOrder: OrderType;
    sender: Profile;
};

export type TicketConnection = {
    __typename?: 'TicketConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<TicketEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Ticket` and its cursor. */
export type TicketEdge = {
    __typename?: 'TicketEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Ticket>;
};

export type Tooth = BusinessLogicNode & {
    __typename?: 'Tooth';
    _id?: Maybe<Scalars['Int']>;
    badColorReason?: Maybe<BadColorReason>;
    cl?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    isBadColor: Scalars['Boolean'];
    relatedService: ServiceType;
    toothNumber: Scalars['Int'];
    toothService?: Maybe<ToothSevice>;
    updatedAt: Scalars['DateTime'];
};

export type ToothConnection = {
    __typename?: 'ToothConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<ToothEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `Tooth` and its cursor. */
export type ToothEdge = {
    __typename?: 'ToothEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<Tooth>;
};

export type ToothMutation = {
    __typename?: 'ToothMutation';
    status?: Maybe<Scalars['String']>;
};

export type ToothMutationJson = {
    __typename?: 'ToothMutationJson';
    status?: Maybe<Scalars['String']>;
};

export type ToothSevice = BusinessLogicNode & {
    __typename?: 'ToothSevice';
    _id?: Maybe<Scalars['Int']>;
    createdAt: Scalars['DateTime'];
    /** The ID of the object */
    id: Scalars['ID'];
    name: Scalars['String'];
    toothSet: ToothConnection;
    updatedAt: Scalars['DateTime'];
};

export type ToothSeviceToothSetArgs = {
    after?: InputMaybe<Scalars['String']>;
    badColorReason_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    before?: InputMaybe<Scalars['String']>;
    cl_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    createdAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
    first?: InputMaybe<Scalars['Int']>;
    id_In?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
    isBadColor_In?: InputMaybe<Array<InputMaybe<Scalars['Boolean']>>>;
    last?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    relatedService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    toothNumber_In?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
    toothService_In?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
    updatedAt_In?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>;
};

export type ToothSeviceConnection = {
    __typename?: 'ToothSeviceConnection';
    edgeCount?: Maybe<Scalars['Int']>;
    /** Contains the nodes in this connection. */
    edges: Array<Maybe<ToothSeviceEdge>>;
    /** Pagination data for this connection. */
    pageInfo: PageInfo;
    totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ToothSevice` and its cursor. */
export type ToothSeviceEdge = {
    __typename?: 'ToothSeviceEdge';
    /** A cursor for use in pagination */
    cursor: Scalars['String'];
    /** The item at the end of the edge */
    node?: Maybe<ToothSevice>;
};

export type UpdateLabRate = {
    __typename?: 'UpdateLabRate';
    rate?: Maybe<Scalars['Float']>;
};

export type UpdateOrder = {
    __typename?: 'UpdateOrder';
    status?: Maybe<Scalars['String']>;
};

export type UpdatePatientPic = {
    __typename?: 'UpdatePatientPic';
    status?: Maybe<Scalars['String']>;
};

export type UpdateProfile = {
    __typename?: 'UpdateProfile';
    status?: Maybe<Scalars['String']>;
};

export type UpdateSmileDesign = {
    __typename?: 'UpdateSmileDesign';
    status?: Maybe<Scalars['String']>;
};

export type Verify = {
    __typename?: 'Verify';
    payload: Scalars['GenericScalar'];
};

export type VerifyUser = {
    __typename?: 'VerifyUser';
    status?: Maybe<Scalars['String']>;
};

export type BusinessLogicNode = {
    /** The ID of the object */
    id: Scalars['ID'];
};

export type ExtendProfileNode = {
    /** The ID of the object */
    id: Scalars['ID'];
};

export type NotificationNode = {
    /** The ID of the object */
    id: Scalars['ID'];
};

export type PatientPics = {
    fullSmileImage?: InputMaybe<Scalars['Upload']>;
    optionalImage?: InputMaybe<Scalars['Upload']>;
    sideImage?: InputMaybe<Scalars['Upload']>;
    smileImage?: InputMaybe<Scalars['Upload']>;
};

export type PatientPicsDeletion = {
    fullSmileImage?: InputMaybe<Scalars['Boolean']>;
    optionalImage?: InputMaybe<Scalars['Boolean']>;
    sideImage?: InputMaybe<Scalars['Boolean']>;
    smileImage?: InputMaybe<Scalars['Boolean']>;
};

export type SmileDesignNode = {
    /** The ID of the object */
    id: Scalars['ID'];
};

export type AddPatientMutationVariables = Exact<{
    address?: InputMaybe<Scalars['String']>;
    age?: InputMaybe<Scalars['Int']>;
    description?: InputMaybe<Scalars['String']>;
    email?: InputMaybe<Scalars['String']>;
    name: Scalars['String'];
    patientPics: PatientPics;
    phoneNumber: Scalars['String'];
    profileDoctorId: Scalars['Int'];
}>;

export type AddPatientMutation = {
    __typename?: 'Mutations';
    createPatient?: { __typename?: 'CreatePatient'; profile?: string | null; user?: string | null } | null;
};

export type GetOrdersQueryVariables = Exact<{
    dr?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
    searchName?: InputMaybe<Scalars['String']>;
    status?: InputMaybe<Scalars['String']>;
}>;

export type GetOrdersQuery = {
    __typename?: 'Query';
    allOrder?: {
        __typename: 'OrderConnection';
        edges: Array<{
            __typename: 'OrderEdge';
            node?: {
                __typename: 'Order';
                _id?: number | null;
                id: string;
                status: BusinesslogicOrderStatusChoices;
                expectedDate?: any | null;
                updatedAt: any;
                relatedService?: {
                    __typename: 'ServiceType';
                    relatedPatient: {
                        __typename: 'PatientType';
                        relatedProfile: {
                            __typename: 'Profile';
                            firstName?: string | null;
                            lastName?: string | null;
                            phoneNumber?: string | null;
                            profilePic?: string | null;
                        };
                    };
                } | null;
            } | null;
        } | null>;
    } | null;
};

export type GetPatientQueryVariables = Exact<{
    id: Scalars['ID'];
}>;

export type GetPatientQuery = {
    __typename?: 'Query';
    Patient?: {
        __typename: 'Patient';
        _id?: number | null;
        patientPic?: {
            __typename: 'PatientPic';
            sideImage?: string | null;
            fullSmileImage?: string | null;
            optionalImage?: string | null;
            smileImage?: string | null;
        } | null;
        relatedProfile: {
            __typename: 'Profile';
            firstName?: string | null;
            lastName?: string | null;
            email?: string | null;
            address?: string | null;
            profilePic?: string | null;
            _id?: number | null;
            age?: number | null;
            phoneNumber?: string | null;
        };
    } | null;
};

export type GetPatientsQueryVariables = Exact<{
    searchName?: InputMaybe<Scalars['String']>;
    profileId?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;

export type GetPatientsQuery = {
    __typename?: 'Query';
    allPatient?: {
        __typename?: 'PatientConnection';
        pageInfo: { __typename?: 'PageInfo'; hasNextPage: boolean; startCursor?: string | null; endCursor?: string | null };
        edges: Array<{
            __typename?: 'PatientEdge';
            node?: {
                __typename?: 'Patient';
                id: string;
                createdAt: any;
                relatedProfile: {
                    __typename?: 'Profile';
                    id: string;
                    firstName?: string | null;
                    lastName?: string | null;
                    phoneNumber?: string | null;
                };
            } | null;
        } | null>;
    } | null;
};

export type LoginMutationVariables = Exact<{
    username: Scalars['String'];
    password: Scalars['String'];
}>;

export type LoginMutation = {
    __typename?: 'Mutations';
    tokenAuth?: { __typename?: 'ObtainJSONWebToken'; token: string; profile?: string | null; refreshToken: string } | null;
};

export type RegisterMutationVariables = Exact<{
    username: Scalars['String'];
    password: Scalars['String'];
    email?: InputMaybe<Scalars['String']>;
}>;

export type RegisterMutation = {
    __typename?: 'Mutations';
    createUser?: {
        __typename?: 'CreateUser';
        user?: string | null;
        profile?: string | null;
        token?: string | null;
        refreshToken?: string | null;
    } | null;
};

export const AddPatientDocument = gql`
    mutation AddPatient(
        $address: String = null
        $age: Int = null
        $description: String = null
        $email: String = ""
        $name: String!
        $patientPics: patientPics!
        $phoneNumber: String!
        $profileDoctorId: Int!
    ) {
        createPatient(
            address: $address
            age: $age
            description: $description
            email: $email
            name: $name
            patientPics: $patientPics
            phoneNumber: $phoneNumber
            profileDoctorId: $profileDoctorId
        ) {
            profile
            user
        }
    }
`;
export type AddPatientMutationFn = Apollo.MutationFunction<AddPatientMutation, AddPatientMutationVariables>;

/**
 * __useAddPatientMutation__
 *
 * To run a mutation, you first call `useAddPatientMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddPatientMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addPatientMutation, { data, loading, error }] = useAddPatientMutation({
 *   variables: {
 *      address: // value for 'address'
 *      age: // value for 'age'
 *      description: // value for 'description'
 *      email: // value for 'email'
 *      name: // value for 'name'
 *      patientPics: // value for 'patientPics'
 *      phoneNumber: // value for 'phoneNumber'
 *      profileDoctorId: // value for 'profileDoctorId'
 *   },
 * });
 */
export function useAddPatientMutation(baseOptions?: Apollo.MutationHookOptions<AddPatientMutation, AddPatientMutationVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useMutation<AddPatientMutation, AddPatientMutationVariables>(AddPatientDocument, options);
}
export type AddPatientMutationHookResult = ReturnType<typeof useAddPatientMutation>;
export type AddPatientMutationResult = Apollo.MutationResult<AddPatientMutation>;
export type AddPatientMutationOptions = Apollo.BaseMutationOptions<AddPatientMutation, AddPatientMutationVariables>;
export const GetOrdersDocument = gql`
    query GetOrders($dr: [String] = null, $searchName: String = null, $status: String = null) {
        allOrder(doctorId: $dr, searchByName: $searchName, statusStartwith: $status) {
            edges {
                node {
                    _id
                    id
                    status
                    expectedDate
                    updatedAt
                    relatedService {
                        relatedPatient {
                            relatedProfile {
                                firstName
                                lastName
                                phoneNumber
                                profilePic
                                __typename
                            }
                            __typename
                        }
                        __typename
                    }
                    __typename
                }
                __typename
            }
            __typename
        }
    }
`;

/**
 * __useGetOrdersQuery__
 *
 * To run a query within a React component, call `useGetOrdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOrdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOrdersQuery({
 *   variables: {
 *      dr: // value for 'dr'
 *      searchName: // value for 'searchName'
 *      status: // value for 'status'
 *   },
 * });
 */
export function useGetOrdersQuery(baseOptions?: Apollo.QueryHookOptions<GetOrdersQuery, GetOrdersQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useQuery<GetOrdersQuery, GetOrdersQueryVariables>(GetOrdersDocument, options);
}
export function useGetOrdersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOrdersQuery, GetOrdersQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useLazyQuery<GetOrdersQuery, GetOrdersQueryVariables>(GetOrdersDocument, options);
}
export type GetOrdersQueryHookResult = ReturnType<typeof useGetOrdersQuery>;
export type GetOrdersLazyQueryHookResult = ReturnType<typeof useGetOrdersLazyQuery>;
export type GetOrdersQueryResult = Apollo.QueryResult<GetOrdersQuery, GetOrdersQueryVariables>;
export const GetPatientDocument = gql`
    query GetPatient($id: ID!) {
        Patient(id: $id) {
            _id
            patientPic {
                sideImage
                fullSmileImage
                optionalImage
                smileImage
                __typename
            }
            relatedProfile {
                firstName
                lastName
                email
                address
                profilePic
                _id
                age
                phoneNumber
                __typename
            }
            __typename
        }
    }
`;

/**
 * __useGetPatientQuery__
 *
 * To run a query within a React component, call `useGetPatientQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPatientQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPatientQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetPatientQuery(baseOptions: Apollo.QueryHookOptions<GetPatientQuery, GetPatientQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useQuery<GetPatientQuery, GetPatientQueryVariables>(GetPatientDocument, options);
}
export function useGetPatientLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPatientQuery, GetPatientQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useLazyQuery<GetPatientQuery, GetPatientQueryVariables>(GetPatientDocument, options);
}
export type GetPatientQueryHookResult = ReturnType<typeof useGetPatientQuery>;
export type GetPatientLazyQueryHookResult = ReturnType<typeof useGetPatientLazyQuery>;
export type GetPatientQueryResult = Apollo.QueryResult<GetPatientQuery, GetPatientQueryVariables>;
export const GetPatientsDocument = gql`
    query GetPatients($searchName: String = null, $profileId: [String] = null) {
        allPatient(searchByName: $searchName, profileId: $profileId) {
            pageInfo {
                hasNextPage
                startCursor
                endCursor
            }
            edges {
                node {
                    id
                    createdAt
                    relatedProfile {
                        id
                        firstName
                        lastName
                        phoneNumber
                    }
                }
            }
        }
    }
`;

/**
 * __useGetPatientsQuery__
 *
 * To run a query within a React component, call `useGetPatientsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPatientsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPatientsQuery({
 *   variables: {
 *      searchName: // value for 'searchName'
 *      profileId: // value for 'profileId'
 *   },
 * });
 */
export function useGetPatientsQuery(baseOptions?: Apollo.QueryHookOptions<GetPatientsQuery, GetPatientsQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useQuery<GetPatientsQuery, GetPatientsQueryVariables>(GetPatientsDocument, options);
}
export function useGetPatientsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPatientsQuery, GetPatientsQueryVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useLazyQuery<GetPatientsQuery, GetPatientsQueryVariables>(GetPatientsDocument, options);
}
export type GetPatientsQueryHookResult = ReturnType<typeof useGetPatientsQuery>;
export type GetPatientsLazyQueryHookResult = ReturnType<typeof useGetPatientsLazyQuery>;
export type GetPatientsQueryResult = Apollo.QueryResult<GetPatientsQuery, GetPatientsQueryVariables>;
export const LoginDocument = gql`
    mutation Login($username: String!, $password: String!) {
        tokenAuth(username: $username, password: $password) {
            token
            profile
            refreshToken
        }
    }
`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const RegisterDocument = gql`
    mutation Register($username: String!, $password: String!, $email: String = "") {
        createUser(username: $username, password: $password, email: $email) {
            user
            profile
            token
            refreshToken
        }
    }
`;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *      email: // value for 'email'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
    const options = { ...defaultOptions, ...baseOptions };
    return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
}
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
