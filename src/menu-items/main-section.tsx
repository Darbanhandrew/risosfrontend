// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconHome, IconUsers, IconNotebook } from '@tabler/icons';

// constant
const icons = {
    IconHome,
    IconUsers,
    IconNotebook
};

// ==============================|| SAMPLE PAGE & DOCUMENTATION MENU ITEMS ||============================== //

const MainSection = {
    id: 'home-',
    type: 'group',
    children: [
        {
            id: 'home',
            title: <FormattedMessage id="home" />,
            type: 'item',
            url: '/home',
            icon: icons.IconHome,
            breadcrumbs: false
        },
        {
            id: 'patients',
            title: <FormattedMessage id="patients" />,
            type: 'item',
            url: '/patients',
            icon: icons.IconUsers,
            breadcrumbs: false
        },
        {
            id: 'orders',
            title: <FormattedMessage id="orders" />,
            type: 'collapse',
            // url: '/orders',
            icon: icons.IconNotebook,
            // breadcrumbs: false
            children: [
                {
                    id: 'orders-processing',
                    title: <FormattedMessage id="orders-processing" />,
                    type: 'item',
                    url: '/orders/processing'
                },
                {
                    id: 'orders-ready',
                    title: <FormattedMessage id="orders-ready" />,
                    type: 'item',
                    url: '/orders/ready'
                },
                {
                    id: 'orders-sent',
                    title: <FormattedMessage id="orders-sent" />,
                    type: 'item',
                    url: '/orders/sent'
                }
            ]
        }
    ]
};

export default MainSection;
