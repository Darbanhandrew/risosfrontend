// project import
// import other from './other';
import MainSection from './main-section';

// types
import { NavItemType } from 'types';

// ==============================|| MENU ITEMS ||============================== //

const menuItems: { items: NavItemType[] } = {
    items: [MainSection]
};

export default menuItems;
