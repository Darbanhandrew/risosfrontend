import React, { useState } from 'react';

// material-ui
import { Avatar, Checkbox, FormControlLabel, Grid, MenuItem, TextField } from '@mui/material';

// project imports
import { gridSpacing } from 'store/constant';
import { GetPatientQuery } from 'generated/graphql';
import DefaultPic from 'assets/images/users/default.png';
import { width } from '@mui/system';
// select options

// ==============================|| PROFILE 2 - BILLING ||============================== //

const Gallery = ({ result }: { result: GetPatientQuery | undefined }) => {
    const [te, setTe] = useState('');

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Frontal Image"
                    src={
                        result?.Patient?.patientPic?.fullSmileImage
                            ? `https://api.risos.ir/mediafiles/${result?.Patient?.patientPic?.fullSmileImage}`
                            : DefaultPic
                    }
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Smile Image"
                    src={
                        result?.Patient?.patientPic?.smileImage
                            ? `https://api.risos.ir/mediafiles/${result?.Patient?.patientPic?.smileImage}`
                            : DefaultPic
                    }
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Side Image"
                    src={
                        result?.Patient?.patientPic?.sideImage
                            ? `https://api.risos.ir/mediafiles/${result?.Patient?.patientPic?.sideImage}`
                            : DefaultPic
                    }
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Optional Image"
                    src={
                        result?.Patient?.patientPic?.optionalImage
                            ? `https://api.risos.ir/mediafiles/${result?.Patient?.patientPic?.optionalImage}`
                            : DefaultPic
                    }
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Frontal Image"
                    src={DefaultPic}
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Avatar
                    alt="Frontal Image"
                    src={DefaultPic}
                    sx={{
                        borderRadius: '16px',
                        width: '200px',
                        height: '200px'
                    }}
                />
            </Grid>
        </Grid>
    );
};

export default Gallery;
