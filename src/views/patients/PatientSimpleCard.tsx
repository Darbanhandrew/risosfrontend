// material-ui
import { useTheme } from '@mui/material/styles';
import { Card, Grid, Typography } from '@mui/material';

import { IconPhone, IconFile, IconCalendar } from '@tabler/icons';

// project imports
import { gridSpacing } from 'store/constant';
import { useIntl } from 'react-intl';

// ==============================|| USER SIMPLE CARD ||============================== //

const UserSimpleCard = ({
    name,
    phone,
    id,
    creationDate,
    status,
    onClick
}: {
    name: string;
    phone: string;
    creationDate: string;
    id: string;
    status: string;
    onClick: () => void;
}) => {
    const theme = useTheme();
    const intl = useIntl();

    return (
        <Card
            sx={{
                p: 2,
                background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
                border: '1px solid',
                borderColor: theme.palette.mode === 'dark' ? 'transparent' : theme.palette.grey[100],
                '&:hover': {
                    border: `1px solid${theme.palette.primary.main}`
                }
            }}
            onClick={onClick}
        >
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12} alignItems="center">
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs zeroMinWidth>
                            <Typography variant="h4">{name}</Typography>
                        </Grid>
                        {/* <Grid item>
                            {status === 'Active' ? (
                                <Chip
                                    label="Active"
                                    size="small"
                                    sx={{
                                        bgcolor: theme.palette.mode === 'dark' ? theme.palette.dark.dark : 'success.light',
                                        color: 'success.dark'
                                    }}
                                />
                            ) : (
                                <Chip
                                    label="Rejected"
                                    size="small"
                                    sx={{
                                        bgcolor: theme.palette.mode === 'dark' ? theme.palette.dark.dark : 'error.light',
                                        color: 'error.dark'
                                    }}
                                />
                            )}
                        </Grid> */}
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item sx={{ display: 'flex', flexDirection: 'row' }} columnSpacing={1}>
                            <IconFile size="20px" />
                            <Typography variant="body2">{id}</Typography>
                        </Grid>
                        <Grid item sx={{ display: 'flex', flexDirection: 'row' }} columnSpacing={1}>
                            <IconCalendar size="20px" />
                            {/* <Typography variant="body2">{phone}</Typography> */}
                            <Typography variant="body2">{intl.formatDate(creationDate)}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
};

export default UserSimpleCard;
