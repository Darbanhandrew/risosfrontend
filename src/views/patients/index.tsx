import React, { useContext } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, Grid, InputAdornment, Menu, MenuItem, OutlinedInput, Pagination, Skeleton, Typography } from '@mui/material';

// project imports
import PatientSimpleCard from './PatientSimpleCard';
import MainCard from 'ui-component/cards/MainCard';
import { gridSpacing } from 'store/constant';

// assets
import { IconSearch } from '@tabler/icons';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import { useGetPatientsQuery } from 'generated/graphql';
import { useIntl } from 'react-intl';
import AuthContext from 'contexts/JWTContext';
import { useNavigate } from 'react-router-dom';

// types

// ==============================|| USER CARD STYLE 2 ||============================== //

const PatientsList = () => {
    const theme = useTheme();
    const context = useContext(AuthContext);
    const navigate = useNavigate();
    const { loading, error, data, refetch } = useGetPatientsQuery({ variables: { profileId: /*  [context?.profile || null] */ null } });
    const intl = useIntl();
    // const dispatch = useDispatch();
    // const [users, setUsers] = React.useState<[]>([]);
    // const { simpleCards } = useSelector((state) => state.user);
    const [anchorEl, setAnchorEl] = React.useState<Element | ((element: Element) => Element) | null | undefined>(null);
    const [search, setSearch] = React.useState<string | undefined>('');
    const [cards, setCards] = React.useState<React.ReactElement | React.ReactElement[]>(<></>);

    const handleClick = (event: React.MouseEvent) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    React.useEffect(() => {
        if (data && data.allPatient && data.allPatient.edges) {
            const n = data.allPatient.edges.map((edge) => (
                <Grid key={edge?.node?.id} item xs={12} sm={6} md={4} lg={3}>
                    <PatientSimpleCard
                        name={`${edge?.node?.relatedProfile.firstName || ''} ${edge?.node?.relatedProfile.lastName || ''}`}
                        phone={edge?.node?.relatedProfile.phoneNumber || ''}
                        id={edge?.node?.relatedProfile.id || ''}
                        creationDate={edge?.node?.createdAt || ''}
                        status="Active"
                        onClick={() => navigate(`/patients/${edge?.node?.id}`)}
                    />
                </Grid>
            ));
            setCards(n);
        }
    }, [data]);

    React.useEffect(() => {
        // setIsLoading(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearch = async (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement> | undefined) => {
        const newString = event?.target.value;
        setSearch(newString);

        if (newString) {
            refetch({ searchName: newString });
        } else {
            refetch();
        }
    };

    if (loading) {
        // show skeleton
        return (
            <MainCard
                title={
                    <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                        <Grid item sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                            <Typography variant="h3" mr={2}>
                                {intl.formatMessage({ id: 'patients' })}
                            </Typography>
                            <Button variant="contained" color="secondary" onClick={() => navigate('/patients/add-patient')}>
                                {intl.formatMessage({ id: 'add_patient' })}
                            </Button>
                        </Grid>
                        <Grid item>
                            <OutlinedInput
                                id="input-search-card-style2"
                                placeholder="Search"
                                value={search}
                                onChange={handleSearch}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <IconSearch stroke={1.5} size="16px" />
                                    </InputAdornment>
                                }
                                size="small"
                            />
                        </Grid>
                    </Grid>
                }
            >
                <Skeleton variant="rectangular" height={gridSpacing * 6} />
            </MainCard>
        );
    }

    return (
        <MainCard
            title={
                <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                    <Grid item sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Typography variant="h3" mr={2}>
                            {intl.formatMessage({ id: 'patients' })}
                        </Typography>
                        <Button variant="contained" color="secondary" onClick={() => navigate('/patients/add-patient')}>
                            {intl.formatMessage({ id: 'add_patient' })}
                        </Button>
                    </Grid>
                    <Grid item>
                        <OutlinedInput
                            id="input-search-card-style2"
                            placeholder="Search"
                            value={search}
                            onChange={handleSearch}
                            startAdornment={
                                <InputAdornment position="start">
                                    <IconSearch stroke={1.5} size="16px" />
                                </InputAdornment>
                            }
                            size="small"
                        />
                    </Grid>
                </Grid>
            }
        >
            <Grid container direction="row" spacing={gridSpacing}>
                {cards}
                <Grid item xs={12}>
                    <Grid container justifyContent="flex-start" spacing={gridSpacing}>
                        <Grid item>
                            <Pagination count={10} color="primary" />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </MainCard>
    );
};

export default PatientsList;
