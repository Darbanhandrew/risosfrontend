// material-ui
import { Grid } from '@mui/material';

// project imports
import AddPatientWizard from './components';
import { gridSpacing } from 'store/constant';

// ==============================|| FORMS WIZARD ||============================== //

const AddPatient = () => (
    <Grid container spacing={gridSpacing} justifyContent="center">
        <Grid item xs={12} md={9} lg={7}>
            <AddPatientWizard />
        </Grid>
    </Grid>
);

export default AddPatient;
