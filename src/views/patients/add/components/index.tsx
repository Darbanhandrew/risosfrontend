import React, { useContext } from 'react';

// material-ui
import { Button, Step, Stepper, StepLabel, Stack, Typography } from '@mui/material';

// project imports
import PatientInfoForm, { UserInfoData } from './PatientInfoForm';
import PaymentForm, { PaymentData } from './PaymentForm';
import Review from './Review';
import MainCard from 'ui-component/cards/MainCard';
import AnimateButton from 'ui-component/extended/AnimateButton';
import { useAddPatientMutation } from 'generated/graphql';
import AuthContext from 'contexts/JWTContext';

// step options
const steps = ['Payment details', 'Patient Basic Info', 'Review your order'];

const getStepContent = (
    step: number,
    handleNext: () => void,
    handleBack: () => void,
    setErrorIndex: (i: number | null) => void,
    userInfo: UserInfoData,
    setUserInfoData: (d: UserInfoData) => void,
    paymentData: PaymentData,
    setPaymentData: (d: PaymentData) => void
) => {
    switch (step) {
        case 1:
            return (
                <PatientInfoForm
                    handleNext={handleNext}
                    setErrorIndex={setErrorIndex}
                    handleBack={handleBack}
                    userInfo={userInfo}
                    setUserInfoData={setUserInfoData}
                />
            );
        case 0:
            return (
                <PaymentForm
                    handleNext={handleNext}
                    setErrorIndex={setErrorIndex}
                    paymentData={paymentData}
                    setPaymentData={setPaymentData}
                />
            );
        case 2:
            return <Review />;
        default:
            throw new Error('Unknown step');
    }
};

// ==============================|| FORMS WIZARD - BASIC ||============================== //

const AddPatientWizard = () => {
    const [addPatientCall, addPatientResp] = useAddPatientMutation();
    const context = useContext(AuthContext);
    const [activeStep, setActiveStep] = React.useState(0);
    const [userInfo, setUserInfo] = React.useState<UserInfoData>({
        patientName: '',
        phoneNumber: '',
        age: '',
        address: '',
        email: ''
    });
    const [paymentData, setPaymentData] = React.useState({});
    const [errorIndex, setErrorIndex] = React.useState<number | null>(null);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
        setErrorIndex(null);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    // const handleSubmit = async () => {
    //     if (context) {
    //         const profileId = context.profile;
    //         if (profileId) {
    //             const { patientName, phoneNumber, age, address, email } = userInfo;

    //             const response = await addPatientCall({
    //                 variables: {
    //                     name: patientName,
    //                     phoneNumber,
    //                     age: parseInt(age, 10),
    //                     address,
    //                     email,
    //                     profileDoctorId: parseInt(profileId, 10),
    //                     patientPics: {
    //                         sideImage: new File(),
    //                         smileImage: new File(),
    //                         fullSmileImage: new File(),
    //                         optionalImage: new File()
    //                     }
    //                 }
    //             });

    //             if (response.data) {
    //                 console.log('Patient added successfully');
    //             } else {
    //                 console.log('Patient not added');
    //             }
    //         }
    //     }
    // };

    return (
        <MainCard title="Validation">
            <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
                {steps.map((label, index) => {
                    const labelProps: { error?: boolean; optional?: React.ReactNode } = {};

                    if (index === errorIndex) {
                        labelProps.optional = (
                            <Typography variant="caption" color="error">
                                Error
                            </Typography>
                        );

                        labelProps.error = true;
                    }

                    return (
                        <Step key={label}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            <>
                {activeStep === steps.length ? (
                    <>
                        <Typography variant="h5" gutterBottom>
                            Thank you for your order.
                        </Typography>
                        <Typography variant="subtitle1">
                            Your order number is #2001539. We have emailed your order confirmation, and will send you an update when your
                            order has shipped.
                        </Typography>
                        <Stack direction="row" justifyContent="flex-end">
                            <AnimateButton>
                                <Button
                                    variant="contained"
                                    color="error"
                                    onClick={() => {
                                        setUserInfo({
                                            patientName: '',
                                            phoneNumber: '',
                                            age: '',
                                            address: '',
                                            email: ''
                                        });
                                        setPaymentData({});
                                        setActiveStep(0);
                                    }}
                                    sx={{ my: 3, ml: 1 }}
                                >
                                    Reset
                                </Button>
                            </AnimateButton>
                        </Stack>
                    </>
                ) : (
                    <>
                        {getStepContent(
                            activeStep,
                            handleNext,
                            handleBack,
                            setErrorIndex,
                            userInfo,
                            setUserInfo,
                            paymentData,
                            setPaymentData
                        )}
                        {activeStep === steps.length - 1 && (
                            <Stack direction="row" justifyContent={activeStep !== 0 ? 'space-between' : 'flex-end'}>
                                {activeStep !== 0 && (
                                    <Button onClick={handleBack} sx={{ my: 3, ml: 1 }}>
                                        Back
                                    </Button>
                                )}
                                <AnimateButton>
                                    <Button variant="contained" onClick={handleNext} sx={{ my: 3, ml: 1 }}>
                                        {activeStep === steps.length - 1 ? 'Place order' : 'Next'}
                                    </Button>
                                </AnimateButton>
                            </Stack>
                        )}
                    </>
                )}
            </>
        </MainCard>
    );
};

export default AddPatientWizard;
