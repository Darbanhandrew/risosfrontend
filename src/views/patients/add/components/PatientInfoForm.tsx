// material-ui
import { Button, Checkbox, FormControlLabel, Grid, Stack, Typography, TextField } from '@mui/material';

// project imports
import AnimateButton from 'ui-component/extended/AnimateButton';

// third-party
import { useFormik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object({
    patientName: yup.string().required('Name is required'),
    phoneNumber: yup.string().required('Phone Number is required'),
    age: yup.string().required('Age is required'),
    address: yup.string(),
    email: yup.string().email('Email is invalid')
});

// ==============================|| FORM WIZARD - VALIDATION  ||============================== //

export type UserInfoData = {
    firstName?: string;
    lastName?: string;
    patientName: string;
    phoneNumber: string;
    age: string;
    address: string;
    email: string;
};

interface PatientInfoFormProps {
    userInfo?: UserInfoData;
    setUserInfoData: (d: UserInfoData) => void;
    handleNext: () => void;
    handleBack: () => void;
    setErrorIndex: (i: number | null) => void;
}

const PatientInfoForm = ({ userInfo, setUserInfoData, handleNext, handleBack, setErrorIndex }: PatientInfoFormProps) => {
    const formik = useFormik({
        initialValues: {
            patientName: userInfo ? userInfo.patientName : '',
            phoneNumber: userInfo ? userInfo.phoneNumber : '',
            age: userInfo ? userInfo.age : '',
            address: userInfo ? userInfo.address : '',
            email: userInfo ? userInfo.email : ''
        },
        validationSchema,
        onSubmit: (values) => {
            setUserInfoData({
                patientName: values.patientName,
                phoneNumber: values.phoneNumber,
                age: values.age,
                address: values.address,
                email: values.email
            });
            handleNext();
        }
    });

    return (
        <>
            <Typography variant="h5" gutterBottom sx={{ mb: 2 }}>
                Patient Info
            </Typography>
            <form onSubmit={formik.handleSubmit} id="validation-forms">
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={9}>
                        <TextField
                            id="name"
                            name="name"
                            label="Patient Name *"
                            value={formik.values.patientName}
                            onChange={formik.handleChange}
                            error={formik.touched.patientName && Boolean(formik.errors.patientName)}
                            helperText={formik.touched.patientName && formik.errors.patientName}
                            fullWidth
                            autoComplete="full-name"
                        />
                    </Grid>
                    <Grid item xs={12} sm={3}>
                        <TextField
                            id="age"
                            name="agee"
                            label="Age *"
                            value={formik.values.age}
                            onChange={formik.handleChange}
                            error={formik.touched.age && Boolean(formik.errors.age)}
                            helperText={formik.touched.age && formik.errors.age}
                            fullWidth
                            autoComplete="age"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="phoneNumber"
                            name="phoneNumber"
                            label="Phone Number"
                            value={formik.values.phoneNumber}
                            onChange={formik.handleChange}
                            error={formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)}
                            helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
                            fullWidth
                            autoComplete="phone-number"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="address"
                            name="address"
                            label="Address"
                            value={formik.values.address}
                            onChange={formik.handleChange}
                            error={formik.touched.address && Boolean(formik.errors.address)}
                            helperText={formik.touched.address && formik.errors.address}
                            fullWidth
                            autoComplete="address"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Stack direction="row" justifyContent="space-between">
                            <Button onClick={handleBack} sx={{ my: 3, ml: 1 }}>
                                Back
                            </Button>
                            <AnimateButton>
                                <Button variant="contained" type="submit" sx={{ my: 3, ml: 1 }} onClick={() => setErrorIndex(1)}>
                                    Next
                                </Button>
                            </AnimateButton>
                        </Stack>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default PatientInfoForm;
