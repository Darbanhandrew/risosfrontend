import React, { useState, useEffect } from 'react';
// material-ui
import { Grid } from '@mui/material';

// project imports
import { gridSpacing } from 'store/constant';
import InfoCard from './InfoCard';
import { FormattedMessage, useIntl } from 'react-intl';
import { Chance } from 'chance';

// ==============================|| SAMPLE PAGE ||============================== //
const chance = new Chance();

const Home = () => {
    const [isLoading, setLoading] = useState(true);
    const intl = useIntl();
    const [processingPatients, setProcessingPatients] = useState(0);
    const [totalPatients, setTotalPatients] = useState(0);
    const [processingOrders, setProcessingOrders] = useState(0);
    const [availableLabs, setAvailableLabs] = useState(0);

    useEffect(() => {
        // time out
        setTimeout(() => {
            setLoading(false);
            setProcessingPatients(chance.integer({ min: 0, max: 100 }));
            setTotalPatients(chance.integer({ min: 0, max: 100 }));
            setProcessingOrders(chance.integer({ min: 0, max: 100 }));
            setAvailableLabs(chance.integer({ min: 0, max: 100 }));
        }, 500);
    }, []);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} md={8}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} container spacing={gridSpacing}>
                        <Grid item md={6} sm={6} xs={12}>
                            <InfoCard
                                isLoading={isLoading}
                                title={intl.formatMessage({ id: 'processing-patients' })}
                                value={processingPatients}
                            />
                        </Grid>
                        <Grid item md={6} sm={6} xs={12}>
                            <InfoCard
                                isLoading={isLoading}
                                title={intl.formatMessage({ id: 'processing-orders' })}
                                value={processingOrders}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} container spacing={gridSpacing}>
                        <Grid item md={6} sm={6} xs={12}>
                            <InfoCard isLoading={isLoading} title={intl.formatMessage({ id: 'total-patients' })} value={totalPatients} />
                        </Grid>
                        <Grid item md={6} sm={6} xs={12}>
                            <InfoCard isLoading={isLoading} title={intl.formatMessage({ id: 'available-labs' })} value={availableLabs} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {/* <Grid item xs={12} md={4}></Grid> */}
        </Grid>
    );
};

export default Home;
