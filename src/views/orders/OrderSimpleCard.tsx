/* eslint-disable no-nested-ternary */
// material-ui
import { useTheme } from '@mui/material/styles';
import { Card, Chip, Grid, Typography } from '@mui/material';

import { IconPhone, IconFile, IconCalendar } from '@tabler/icons';

// project imports
import { gridSpacing } from 'store/constant';
import { useIntl } from 'react-intl';

// ==============================|| USER SIMPLE CARD ||============================== //

const OrderSimpleCard = ({
    name,
    status,
    id,
    phone,
    expectationDate,
    onClick
}: {
    name: string;
    phone: string;
    expectationDate: string;
    id: string;
    status: 'SENT' | 'READY' | 'PROCESSING';
    onClick: () => void;
}) => {
    const theme = useTheme();
    const intl = useIntl();

    return (
        <Card
            sx={{
                p: 2,
                background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
                border: '1px solid',
                borderColor: theme.palette.mode === 'dark' ? 'transparent' : theme.palette.grey[100],
                '&:hover': {
                    border: `1px solid${theme.palette.primary.main}`
                }
            }}
            onClick={onClick}
        >
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12} alignItems="center">
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs zeroMinWidth>
                            <Typography variant="h4">{name}</Typography>
                        </Grid>
                        <Grid item>
                            <Chip
                                label={status}
                                size="small"
                                sx={{
                                    bgcolor:
                                        theme.palette.mode === 'dark'
                                            ? theme.palette.dark.dark
                                            : status === 'SENT'
                                            ? 'success.light'
                                            : status === 'READY'
                                            ? 'warning.light'
                                            : 'error.light',
                                    color: status === 'SENT' ? 'success.dark' : status === 'READY' ? 'warning.dark' : 'error.dark'
                                }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item sx={{ display: 'flex', flexDirection: 'row' }} columnSpacing={1}>
                            <IconFile size="20px" />
                            <Typography variant="body2">{id}</Typography>
                        </Grid>
                        <Grid item sx={{ display: 'flex', flexDirection: 'row' }} columnSpacing={1}>
                            <IconCalendar size="20px" />
                            {/* <Typography variant="body2">{phone}</Typography> */}
                            <Typography variant="body2">{intl.formatDate(expectationDate)}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
};

export default OrderSimpleCard;
