import { lazy } from 'react';

// project imports
import AuthGuard from 'utils/route-guard/AuthGuard';
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';

// sample page routing
const Home = Loadable(lazy(() => import('views/home')));
const Patients = Loadable(lazy(() => import('views/patients')));
const AddPatient = Loadable(lazy(() => import('views/patients/add')));
const Patient = Loadable(lazy(() => import('views/patients/patient/index')));
const Orders = Loadable(lazy(() => import('views/orders')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: (
        <AuthGuard>
            <MainLayout />
        </AuthGuard>
    ),
    children: [
        {
            path: '/',
            element: <Home />
        },
        {
            path: '/home',
            element: <Home />
        },
        {
            path: '/patients',
            element: <Patients />
        },
        {
            path: '/patients/:id',
            element: <Patient />
        },
        {
            path: '/patients/add-patient',
            element: <AddPatient />
        },
        {
            path: '/orders/processing',
            element: <Orders />
        },
        {
            path: '/orders/ready',
            element: <Orders />
        },
        {
            path: '/orders/sent',
            element: <Orders />
        }
    ]
};

export default MainRoutes;
