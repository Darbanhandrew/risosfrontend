// third-party
import firebase from 'firebase/compat/app';

// project imports
import { UserProfile } from 'types/user-profile';


export interface JWTDataProps {
    userId: string;
}

export type JWTContextType = {
    isLoggedIn: boolean;
    isInitialized?: boolean;
    user?: UserProfile | null | undefined;
    userId?: string | null | undefined;
    profile?: string | null | undefined;
    logout: () => void;
    login: (username: string, password: string) => Promise<void>;
    register: (email: string, password: string, username: string) => Promise<void>;
    resetPassword: (email: string) => void;
    updateProfile: VoidFunction;
};

export interface InitialLoginContextProps {
    isLoggedIn: boolean;
    isInitialized?: boolean;
    user?: UserProfile | null | undefined;
    profile?: string | null | undefined;
    userId?: string | null | undefined;
}
