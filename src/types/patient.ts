export interface Patient {
    _id: number;
    patientPic: PatientPic;
    relatedProfile: RelatedProfile;
    __typename: string;
}

export interface PatientPic {
    sideImage: string;
    fullSmileImage: string;
    optionalImage: string;
    smileImage: string;
    __typename: string;
}

export interface RelatedProfile {
    firstName: string;
    lastName: string | null;
    email: string;
    address: string;
    profilePic: string;
    _id: number;
    age: number;
    phoneNumber: string;
    __typename: string;
}
